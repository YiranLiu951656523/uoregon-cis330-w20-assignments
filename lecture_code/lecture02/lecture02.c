#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PI 3.141592653589793238;
#define CIRCLE_AREA(r) (3.14 * r * r)

#ifdef USE_DOUBLE
typedef double ftype;
#else
typedef float ftype;
#endif

#define MAX_NAME_LENGTH 1000
struct human_struct {
    char name[MAX_NAME_LENGTH];
    unsigned int age;
};


typedef struct creature_struct {
    char* species;
    char* name;
    unsigned int age;
} creature_t;

int main(int argc, char** argv)
{
    // ------------------------------------------------------------------------
    // Printf
    double some_num = PI;
    double some_num_other = 300.141592653589793238;
    // print a floating point number
    fprintf(stdout, "some_num is %f\n", some_num); 
    // print the first 10 decimal digits
    fprintf(stdout, "some_num is %.10f\n\n", some_num);


    // print the first 10 decimal digits
    fprintf(stdout, "some_num_other is %.10f\n", some_num_other);
    // print the first 10 decimal digits, but in a 20-wide space, right justified
    fprintf(stdout, "some_num_other is %20.10f\n", some_num_other);
    // print the first 10 decimal digits, but in a 20-wide space, left justified
    fprintf(stdout, "some_num_other is %-20.10f\n\n", some_num_other);

    // print in scientific notation
    fprintf(stdout, "some_num_other is %.10e\n\n", some_num_other);
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // i++ vs. ++i
    int some_int = 10;
    fprintf(stdout, "some_int is %d\n", some_int);
    fprintf(stdout, "some_int++ is %d\n", some_int++);
    fprintf(stdout, "++some_int is %d\n\n", ++some_int);
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // ternary operations
    int first_cond = 0; 
    int second_cond = 1; 
    (first_cond == second_cond) ? printf("hello\n") : printf("world\n");
    int final_cond = (first_cond == second_cond) ? first_cond : ++second_cond;
    fprintf(stdout, "final_cond is %d\n\n", final_cond);
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // Macros
    double radius = 1.0;
    double area = CIRCLE_AREA(radius + radius);
    fprintf(stdout, "area of the circle is %f\n\n", area);    
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // File I/O
    FILE* fp = fopen("example.txt", "r");
    char line[100];
    while(fgets(line, 100, fp) != NULL) {
        fprintf(stdout, "%s", line);
    }
    fprintf(stdout, "\n");
    fclose(fp);
    fp = fopen("example.txt", "a");
    fprintf(fp, "John is a 21 year old human\n");
    fclose(fp);
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // typedef and compound data structures
    // ------------------------------------------------------------------------
    // compile with and without -DUSE_DOUBLE
    ftype some_float_num = 1.0;
    fprintf(stdout, "sizeof ftype is %ld\n", sizeof(some_float_num));

    struct human_struct human_a;
    strcpy(human_a.name, "John");
    human_a.age = 21;
    fprintf(stdout, "%s is %d years old\n", human_a.name, human_a.age);

    creature_t* creature_a;
    creature_a = (creature_t*) malloc(sizeof(creature_t));   
    creature_a->species = (char*) malloc(sizeof(char) * MAX_NAME_LENGTH);
    creature_a->name = (char*) malloc(sizeof(char) * MAX_NAME_LENGTH);

    strcpy(creature_a->species, "Human");
    strcpy(creature_a->name, "John");
    creature_a->age = 21;
    fprintf(stdout, "%s is %d years old %s\n", creature_a->name, creature_a->age,
            creature_a->species);

    free(creature_a->species);
    free(creature_a->name);
    free(creature_a);
    // ------------------------------------------------------------------------

    return 0;
}
